Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Rajshekar",
    "job": "SM"
}

Response header date is : 
Fri, 08 Mar 2024 08:52:15 GMT

Response body is : 
{"name":"Rajshekar","job":"SM","id":"567","createdAt":"2024-03-08T08:52:15.363Z"}