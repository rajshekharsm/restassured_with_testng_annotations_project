Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Rajshekar",
    "job": "SM"
}

Response header date is : 
Fri, 08 Mar 2024 08:45:06 GMT

Response body is : 
{"name":"Rajshekar","job":"SM","id":"390","createdAt":"2024-03-08T08:45:06.323Z"}