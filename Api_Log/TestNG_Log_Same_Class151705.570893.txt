Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 08 Mar 2024 09:47:06 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"133","createdAt":"2024-03-08T09:47:06.400Z"}