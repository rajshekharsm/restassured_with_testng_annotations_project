Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Vinayak",
    "job": "Qa"
}

Response header date is : 
Fri, 08 Mar 2024 07:47:53 GMT

Response body is : 
{"name":"Vinayak","job":"Qa","id":"835","createdAt":"2024-03-08T07:47:52.974Z"}