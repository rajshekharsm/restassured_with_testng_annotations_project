Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Rajshekhar",
    "job": "PO"
}

Response header date is : 
Sun, 03 Mar 2024 01:07:27 GMT

Response body is : 
{"name":"Rajshekhar","job":"PO","id":"457","createdAt":"2024-03-03T01:07:27.364Z"}