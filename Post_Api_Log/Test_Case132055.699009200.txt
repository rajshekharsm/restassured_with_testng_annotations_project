Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 08 Mar 2024 07:50:56 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"373","createdAt":"2024-03-08T07:50:56.512Z"}