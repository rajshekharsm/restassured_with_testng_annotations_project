package Test_Package_Annotations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import Repository.Request_Body;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class TestNG_Annotations {
	String requestBody;
	String Endpoint;
	File dir_name;
	Response response;
	// response_post, response_put;

	@BeforeTest
	public void testsetup() throws IOException {
		dir_name = Utility.CreateLogDirectory("Api_Log");

		requestBody = Request_Body.Req_Tc1();

		Endpoint = Request_Body.Hostname() + Request_Body.Resource();

	}

	@Test(description = "Validate the response body parameters of API")
	public void validator() throws IOException {

		System.out.println("Test Method Called");
		response = API_Trigger.Post_trigger(Request_Body.Headername(), Request_Body.Headervalue(), requestBody,
				Endpoint);
		int statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();

		String res_name = (response.jsonPath().getString("name"));
		System.out.println(res_name);
		String res_job = (response.jsonPath().getString("job"));
		System.out.println(res_job);
		String res_id = (response.jsonPath().getString("id"));
		System.out.println(res_id);
		String res_createdAt = response.jsonPath().getString("createdAt").substring(0, 11);
		System.out.println(res_createdAt);

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdAt, expecteddate);
		Assert.assertNotNull(res_id);
		Utility.evidenceFileCreator(Utility.testLogName("TestNg_Log"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
	}

	@AfterTest
	public void evidenceCreator() throws IOException {
		System.out.println("After Test Method Called");

	}

}
