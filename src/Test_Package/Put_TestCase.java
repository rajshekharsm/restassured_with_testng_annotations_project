package Test_Package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Request_Body;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Put_TestCase extends Request_Body {

	@Test
	public static void execute() throws IOException {

		RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());

		File dir_name = Utility.CreateLogDirectory("Put_Api_Log");

		String requestbody = Request_Body.Req_Put_TestCase();

		String Endpoint = Request_Body.Hostname() + Request_Body.Resource_Put_TestCase();

		Response response = API_Trigger.PUT_Trigger(Request_Body.Headername(), Request_Body.Headervalue(),
				Request_Body.Req_Put_TestCase(), Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("TestCase_3_put_method"), dir_name, Endpoint,
				Request_Body.Req_Put_TestCase(), response.getHeader("Date"), response.getBody().asString());

		String res_body = (response.getBody().asString());
		System.out.println(res_body);
		int statuscode = (response.statusCode());
		System.out.println(statuscode);
		String res_name = (response.jsonPath().getString("name"));
		System.out.println(res_name);
		String res_job = (response.jsonPath().getString("job"));
		System.out.println(res_job);
		String res_updatedAt = response.jsonPath().getString("updatedAt").substring(0, 11);
		System.out.println(res_updatedAt);

		// STEP 3: PARSE THE REQUEST BODY AND SAVE IT INTO LOCAL VARIABLES
		// STEP 3.1: CREATE THE OBJECT FOR THE REQUEST BODY

		JsonPath jsp_req = new JsonPath(Request_Body.Req_Put_TestCase());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// STEP 4: GENERATE EXPECTED DATE

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// STEP 5: USE testNG ASSERT

		Assert.assertEquals(response.jsonPath().getString("name"), req_name);
		Assert.assertEquals(response.jsonPath().getString("job"), req_job);
		Assert.assertEquals(response.jsonPath().getString("updatedAt").substring(0, 11), expecteddate);

	}

}
