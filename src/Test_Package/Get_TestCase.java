package Test_Package;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Request_Body;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Get_TestCase extends Request_Body {

	@Test
	public static void execute() throws IOException {

		RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());

		File Dir_Name = Utility.CreateLogDirectory("Get_Api_Log");

		String Endpoint = Request_Body.Hostname() + Request_Body.Resource_Get_Testcase();

		Response response = API_Trigger.Get_Trigger(Request_Body.Headername(), Request_Body.Headervalue(), Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("TestCase_5_Get_method"), Dir_Name, Endpoint,
				Request_Body.Req_Get_TestCase(), response.getHeader("Date"), response.getBody().asPrettyString());
		// PRINT RESPONSE AND STATUS CODE

		System.out.println(response.getBody().asPrettyString());
		String res_body = response.getBody().asPrettyString();
		int status_code = response.statusCode();
		System.out.println(status_code);

		// CREATE JSON OBJECT

		JsonPath res_jsn = new JsonPath(res_body);

		// EXTRACT LENGTH OF DATA ARRAY

		int count = res_jsn.getInt("data.size()");
		System.out.println(count);
		int id = 0;
		int idArr[] = new int[count];
		int ids[] = { 7, 8, 9, 10, 11, 12 };

		// EXTRACT RESPONSE BODY PARAMETER

		for (int i = 0; i < count; i++) {

			id = res_jsn.getInt("data[" + i + "].id");
			System.out.println(id);
			idArr[i] = id;

			String res_firstName = res_jsn.getString("data[" + i + "].first_name");
			System.out.println(res_firstName);

			String res_lastName = res_jsn.getString("data[" + i + "].last_name");
			System.out.println(res_lastName);

			String res_email = res_jsn.getString("data[" + i + "].email");
			System.out.println(res_email);
		}

		System.out.println("------------------------------");
		for (int i = 0; i < idArr.length; i++) {
			System.out.println(idArr[i]);

			// VALIDATE RESPONSE BODY PARAMETER:

			Assert.assertEquals(ids[i], idArr[i]);
		}
	}

}
