package Test_Package;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Repository.Request_Body;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Delete_TestCase extends Request_Body {
@Test
	public static void execute () {
		RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());

		String Endpoint = Request_Body.Hostname() + Request_Body.Resource_Delect_Testcase();

		Response response = API_Trigger.Delete_Trigger(Request_Body.Hostname(), Request_Body.Headervalue(), Endpoint);

		System.out.println(response.getBody().asPrettyString());

		int status_code = response.statusCode();
		System.out.println(status_code);

		// VALIDATE RESPONSE CODE USING TESTNG ASSERT:

		Assert.assertEquals(status_code, 204);

	}

}
