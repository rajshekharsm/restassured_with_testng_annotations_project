package Test_Package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Request_Body;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_TestCase_2 extends Request_Body {

	@Test
	public static void execute() throws IOException {

		RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());

		File dir_Name = Utility.CreateLogDirectory("Post_Api_Log");
		
		String requestBody = Request_Body.Req_Tc2();

		String Endpoint = Request_Body.Hostname() + Request_Body.Resource();
		int statuscode = 0;
		for (int i = 0; i < 5; i++) {

			Response response = API_Trigger.Post_trigger(Request_Body.Headername(), Request_Body.Headervalue(),
					Request_Body.Req_Tc2(), Endpoint);
			statuscode = response.statusCode();
			if (statuscode == 201) {

				Utility.evidenceFileCreator(Utility.testLogName("Post_TestCase_2"), dir_Name, Endpoint,
						Request_Body.Req_Tc2(), response.getHeader("Date"), response.getBody().asString());
				
				validator(response, requestBody);
				break;
			} else {
				System.out.println("Expected Statuscode not found in current iteration:" + i + "Hence retry");
			}
		}
		if (statuscode != 201) {
			System.out.println("Expected statuscode not found Even after 5 retries hence Failing the Testcase");
			Assert.assertEquals(statuscode, 200);
		}
	}

	public static void validator(Response response, String requestBody) throws IOException {
		ResponseBody res_body = response.getBody();

		String res_name = (response.jsonPath().getString("name"));
		System.out.println(res_name);
		String res_job = (response.jsonPath().getString("job"));
		System.out.println(res_job);
		String res_id = (response.jsonPath().getString("id"));
		System.out.println(res_id);
		String res_createdAt = response.jsonPath().getString("createdAt").substring(0, 11);
		System.out.println(res_createdAt);

		// STEP 3: PARSE THE REQUEST BODY AND SAVE IT INTO LOCAL VARIABLES
		// STEP 3.1: CREATE THE OBJECT FOR THE REQUEST BODY

		JsonPath jsp_req = new JsonPath(Request_Body.Req_Tc2());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// STEP 4: GENERATE EXPECTED DATE

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// STEP 5: USE testNG ASSERT

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdAt, expecteddate);
		Assert.assertNotNull(res_id);
	}

}
