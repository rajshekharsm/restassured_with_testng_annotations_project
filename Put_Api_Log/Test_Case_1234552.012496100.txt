Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "zion resident"
}

Response header date is : 
Sat, 02 Mar 2024 18:15:53 GMT

Response body is : 
{"name":"morpheus","job":"zion resident","updatedAt":"2024-03-02T18:15:53.025Z"}