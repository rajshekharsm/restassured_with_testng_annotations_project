Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "zion resident"
}

Response header date is : 
Tue, 05 Mar 2024 11:36:59 GMT

Response body is : 
{"name":"morpheus","job":"zion resident","updatedAt":"2024-03-05T11:36:59.147Z"}