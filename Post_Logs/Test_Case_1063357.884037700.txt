Endpoint is :
https://reqres.in/api/users

Request body is :
{
    " name": "morpheus",
    "job": "leader"
}

Response header date is : 
Sun, 03 Mar 2024 01:03:58 GMT

Response body is : 
{" name":"morpheus","job":"leader","id":"234","createdAt":"2024-03-03T01:03:57.958Z"}